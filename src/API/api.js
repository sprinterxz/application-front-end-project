import axios from "axios";
axios.defaults.baseURL = "https://dummyjson.com/products?limit=3";

export default {
    getData() {
        return axios.get().then(resp => {
            return resp.data
        }).catch(err => {
            throw err
        })
    }
}